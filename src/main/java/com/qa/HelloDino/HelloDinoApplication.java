package com.qa.HelloDino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloDinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloDinoApplication.class, args);
	}

}
